#!/usr/bin/env node
import program from "commander";
import chalk from "chalk";
import clipboardy from "clipboardy";
const log = console.log;
import { createIban, createBic } from "./utils/createIban.js";
import { saveIban } from "./utils/saveIban.js";
program.version("1.0.0").description("Simple Iban and Bic Generator");

program
  .option("-c, --country <string>", "Country Code exemple(GE,AT) FR by default")
  .option("-s, --save", "save iban to iban.txt")
  .option("-si, --saveIbanClipboardy", "save iban to iban.txt")
  .option("-sb, --saveBic", "save iban to bic.txt")
  .parse();

const { country, save, saveIbanClipboardy, saveBic } = program.opts();

// Get generated iban
const generatedIban = createIban(country);
const generateBic = createBic(country);

//  Save to file
if (save) saveIban(generatedIban, generateBic);

//  Copy to clipboard
if (saveIbanClipboardy) clipboardy.writeSync(generatedIban);
if (saveBic) clipboardy.writeSync(generateBic);

// Output generator password
log(chalk.blue("Generated iban: ") + chalk.bold(generatedIban));
log(chalk.cyanBright("Generated bic: ") + chalk.bold(generateBic));
if (saveBic) log(chalk.yellow("Bic copied to clipboard"));
if (saveIbanClipboardy) log(chalk.yellow("Iban copied to clipboard"));
