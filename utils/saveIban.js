"use strict";
import fs from "fs-extra";
import * as path from "path";
import os from "os";
import chalk from "chalk";

const saveIban = (iban, bic) => {
  // full_path = os.path.realpath(__file__);
  console.log(process.cwd());
  fs.open(path.join(process.cwd(), "iban.txt"), "a", (e, id) => {
    fs.write(
      id,
      "Iban: " + iban + "  Bic: " + bic + os.EOL,
      null,
      "utf-8",
      () => {
        fs.close(id, () => {
          console.log(chalk.green("Password saved to iban.txt"));
        });
      }
    );
  });
};

export { saveIban };
