import { IBANBuilder, IBAN, BIC, BbanStructure } from "ibankit";

const alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const numbers = "0123456789";

const createIban = (country = "FR") => {
  let datas = {};
  for (const items in BbanStructure) {
    datas = BbanStructure[items][country.toUpperCase()];
  }
  let bankCodeLength = 0;
  if (datas["entries"].find(({ entryType }) => entryType === 0)) {
    bankCodeLength = datas["entries"].find(({ entryType }) => entryType === 0)[
      "length"
    ];
  }
  let characterAlpha = 0;
  if (datas["entries"].find(({ entryType }) => entryType === 0)) {
    characterAlpha = datas["entries"].find(({ entryType }) => entryType === 0)[
      "characterType"
    ];
  }

  let accountNumberLength = 0;
  if (datas["entries"].find(({ entryType }) => entryType === 2)) {
    accountNumberLength = datas["entries"].find(
      ({ entryType }) => entryType === 2
    )["length"];
  }

  let bankCode = "";
  if (characterAlpha === 1) {
    for (let i = 0; i < bankCodeLength; i++) {
      bankCode += alpha.charAt(Math.floor(Math.random() * alpha.length));
    }
  } else {
    for (let i = 0; i < bankCodeLength; i++) {
      bankCode += numbers.charAt(Math.floor(Math.random() * numbers.length));
    }
  }

  let accountNumber = "";
  for (let i = 0; i < accountNumberLength; i++) {
    accountNumber += numbers.charAt(Math.floor(Math.random() * numbers.length));
  }

  const customCountryCode = country.toUpperCase();
  const iban = new IBANBuilder()
    .countryCode(customCountryCode)
    .bankCode(bankCode.toUpperCase())
    .accountNumber(accountNumber.toUpperCase())
    .build()
    .toString();
  const validateIban = IBAN.isValid(iban);
  if (!!validateIban === true) return iban;
};
const createBic = (country = "FR") => {
  let bic = "";
  const bankCode = 4;
  let locationCoding = "";
  for (let i = 0; i < bankCode; i++) {
    bic += alpha.charAt(Math.floor(Math.random() * alpha.length));
  }
  for (let i = 0; i < 1; i++) {
    locationCoding += alpha.charAt(Math.floor(Math.random() * alpha.length));
  }
  let reversed = "";
  const str = `${locationCoding}${Math.floor(Math.random() * (1 + 1) + 1)}`;
  for (let i = str.length - 1; i >= 0; i--) {
    reversed += str[i];
  }

  bic = `${bic}${country}${locationCoding}${Math.floor(
    Math.random() * (1 + 1) + 1
  )}`.toUpperCase();
  const validateBic = BIC.isValid(bic);
  if (!!validateBic === true) return bic;
};

export { createIban, createBic };
