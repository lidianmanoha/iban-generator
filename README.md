Create new iban and bic 
CMD -> node app.js OR iban

Create new iban 
CMD -> iban --country=be  or -c be

Create new iban and save to iban.txt
CMD -> iban  --save or -s

Create new iban and save to clipboard
CMD -> iban  --saveIbanClipboardy or -si

Create new bic and save to clipboard
CMD -> bic  --saveBic or -sb

HELP -> -h